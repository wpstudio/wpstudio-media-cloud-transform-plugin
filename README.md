# Bust your current installed site with media cloud s3

if you have a many file uploaded to local website dir, you might help with this plugin.

For install plugin via composer you need preinstalled composer/installers

## How transform?

After install plugin - navigate in media cloud setting "Media cloud -> Convert local to S3"

This solution converting only uploads metadata. At that moment wpstudio-media-cloud-transform not automatically uploads your local files to S3-server.
It means you need manually copy files to s3 server, example in "media" bucket.

# S3 minio file structure

Create bucket "media" on your S3 minio server. 

Sync `wp-content/uploads/` to `_data/media/` on your s3-volume.
