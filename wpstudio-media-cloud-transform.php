<?php
/**
 * Plugin Name:     Wpstudio Media Cloud Transform
 * Plugin URI:      PLUGIN SITE HERE
 * Description:     PLUGIN DESCRIPTION HERE
 * Author:          YOUR NAME HERE
 * Author URI:      YOUR SITE HERE
 * Text Domain:     wpstudio-media-cloud-transform
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Wpstudio_Media_Cloud_Transform
 */

use WpsMcloud\Actions;
use WpsMcloud\Exceptions\VendorAutoloadException;
use WpsMcloud\Pages;
use WpsMcloud\Logger;

define('WPSTUDIO_MEDIA_CLOUD_TRANFORM_BASE_PATH', dirname(__FILE__));

add_action('admin_menu', fn () => add_submenu_page(
    'media-cloud',
    'Convert local to S3',
    'Convert local to S3',
    'manage_options',
    'convert_local_to_s3',
    function () {
//        $ilabLoader = require dirname(__FILE__) . '/../ilab-media-tools/vendor/autoload.php';
//
//        $ilabClassLoader = new Composer\Autoload\ClassLoader();
//        $ilabLoader->unregister();
        include __DIR__ . '/src/Support/helpers.php';

        $localVendorAutoloadPath = 'vendor/autoload.php';
        $projectVendorAutoloadPath = ABSPATH . 'vendor/autoload.php';

        if (file_exists($localVendorAutoloadPath)) {
            include $localVendorAutoloadPath;
        } elseif (file_exists($projectVendorAutoloadPath)) {
            include $projectVendorAutoloadPath;
        } else {
            throw new VendorAutoloadException('Unable to find appropriate composer autoloader');
        }

        $dotEnvFile = __DIR__ . '/.env';

        if (file_exists($dotEnvFile)) {
            $dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
            $dotenv->load();
        }

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $loggerConfig = include __DIR__ . '/config/logger.php';

            $logger = new Logger($loggerConfig['log_type']);

            echo '<pre>';
            (new Actions\Convert())->run();
            echo '</pre>';
        }

        (new Pages\Converter())->render();
    },
    6
), 99);
