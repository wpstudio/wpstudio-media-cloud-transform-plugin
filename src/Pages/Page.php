<?php namespace WpsMcloud\Pages;

use WpsMcloud\Exceptions\ViewNotFoundException;

abstract class Page
{
    private static string $viewsDir = 'views';

    /**
     * @throws ViewNotFoundException
     */
    public function render(): void
    {
        $this->renderView();
    }

    /**
     * @throws ViewNotFoundException
     */
    private function renderView(): void
    {
        $lowerCaseLastPartThisClassName = strtolower(array_reverse(explode('\\', get_class($this)))[0]);

        $filePathWithoutExtension = self::getViewsPath() . DIRECTORY_SEPARATOR  .$lowerCaseLastPartThisClassName;

        $allowedExtensions = [
            'html',
            'php'
        ];

        $fileIsFound = false;

        while (!$fileIsFound && $currentExtension = current($allowedExtensions)) {
            if (file_exists($filePath = $filePathWithoutExtension . '.' . $currentExtension)) {
                $fileIsFound = true;

                include $filePath;
            }
        }

        if (!$fileIsFound) {
            throw new ViewNotFoundException(sprintf(
                'Not found any view file in view dir "%s" for this controller "%s" with these available extension: %s',
                self::getViewsPath(),
                $lowerCaseLastPartThisClassName,
                implode(', ', $allowedExtensions)
            ));
        }
    }

    private static function getViewsPath(): string
    {
        return WPSTUDIO_MEDIA_CLOUD_TRANFORM_BASE_PATH . DIRECTORY_SEPARATOR . self::$viewsDir;
    }
}
