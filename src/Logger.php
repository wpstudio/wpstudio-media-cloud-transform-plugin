<?php namespace WpsMcloud;

use WpsMcloud\Exceptions\LoggerException;

class Logger
{
    const LOG_TYPE_HTML = 'html';
    const LOG_TYPE_ERROR_LOG = 'error_log';
    const LOG_TYPE_BOTH = 'both';

    private static array $logTypes = [
        self::LOG_TYPE_HTML,
        self::LOG_TYPE_ERROR_LOG,
        self::LOG_TYPE_BOTH,
    ];

    private string $logType;

    /**
     * @param string $logType
     * @throws LoggerException
     */
    public function __construct(string $logType)
    {
        $this->validateLogType($logType);

        $this->logType = $logType;
    }

    /**
     * @param string $logType
     * @throws LoggerException
     */
    private function validateLogType(string $logType): void
    {
        if (!in_array($logType, self::$logTypes)) {
            throw new LoggerException('Log type wrong: ' . $logType);
        }
    }

    public function log(string $message, bool $newLineAfter = true): void
    {
        switch ($this->logType) {
            case self::LOG_TYPE_HTML:
                $this->outputToDefaultStream($message, $newLineAfter);
                break;
            case self::LOG_TYPE_ERROR_LOG:
                $this->outputToPhpErrorLog($message);
                break;
            case self::LOG_TYPE_BOTH:
                $this->outputToDefaultStream($message, $newLineAfter);

                $this->outputToPhpErrorLog($message);
                break;
        }
    }

    private function outputToDefaultStream(string $message, bool $newLineAfter): void
    {
        echo $message;

        if ($newLineAfter) {
            echo PHP_EOL;
        }
    }

    private function outputToPhpErrorLog(string $message): void
    {
        error_log($message);
    }

    public function openPreformating()
    {
        echo '<pre>';
    }

    public function closePreformating()
    {
        echo '</pre>';
    }
}
