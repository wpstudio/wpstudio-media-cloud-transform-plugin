<?php

if (!function_exists('mb_basename')) {
    function mb_basename(string $file)
    {
        $filenameParts = explode('/', $file);

        return end($filenameParts);
    }
}
