<?php namespace WpsMcloud\Support;

use Guzzle\Http\Client;
use Guzzle\Http\Exception\ClientErrorResponseException;

class Http
{
    private static Client $guzzle;

    private static function getGuzzle(): Client
    {
        if (!isset(static::$guzzle)) {
            static::$guzzle = new Client();
        }

        return static::$guzzle;
    }

    public static function fileExists(string $url, ?\Closure $successCallback = null, ?\Closure $errorCallback = null): bool
    {

        try {
            $res = self::getGuzzle()->createRequest('HEAD', $url, null, null, [
                'allow_redirects' => true,
            ])->send();

            if ($res->getStatusCode() == 200) {
                if ($successCallback !== null) {
                    $successCallback();
                }

                return true;
            }
        } catch (ClientErrorResponseException $e) {
            if ($errorCallback !== null) {
                throw $e;
            }
        }

        if ($errorCallback !== null) {
            $errorCallback();
        }

        return false;
    }
}
