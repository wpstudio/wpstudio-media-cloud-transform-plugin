<?php namespace WpsMcloud\Actions;

use WpsMcloud\Actions\Convert\Attachment;
use WpsMcloud\Actions\Convert\ReplacerService;
use WpsMcloud\Support\Counters;

class Convert
{
    private Attachment $attachment;

    private ReplacerService $replacer;

    public static int $countProcessedImages = 0;

    public static int $countImagesNotFound = 0;

    public function __construct()
    {
        $this->attachment = new Attachment();

        $this->replacer = new ReplacerService();
    }

    public function run()
    {
        $images = $this->attachment->getImagesWithoutS3MetaData();

        echo sprintf('Count for the processing: %d<br>', count($images));

        foreach ($images as $post) {
            $this->replacer->processImage($post);
        }

        echo sprintf('Processed: %d<br>', Counters::$processedImages);
        echo sprintf('Not found: %d<br>', Counters::$imagesNotFound);
    }
}
