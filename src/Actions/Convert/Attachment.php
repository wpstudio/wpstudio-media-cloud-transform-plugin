<?php namespace WpsMcloud\Actions\Convert;

use WpsMcloud\Models\PostAttachment;

class Attachment
{
    public function getImagesWithoutS3MetaData(): array
    {
        global $wpdb;

        $query = <<<EOF
SELECT $wpdb->posts.ID, $wpdb->posts.guid, $wpdb->posts.post_mime_type, $wpdb->postmeta.meta_id, $wpdb->postmeta.meta_value
FROM $wpdb->posts
    JOIN $wpdb->postmeta ON $wpdb->posts.ID=$wpdb->postmeta.post_id
    LEFT JOIN $wpdb->postmeta pm2 ON $wpdb->posts.ID=pm2.post_id AND pm2.meta_key = %s
WHERE
    $wpdb->posts.post_type = %s and
    $wpdb->postmeta.meta_key = %s and 
    $wpdb->postmeta.meta_value not like %s and
    pm2.meta_id IS NULL
EOF;

        return PostAttachment::get($wpdb->get_results($wpdb->prepare(
            $query,
            [
                PostAttachment::META_KEY_FLAGGED_AS_404,
                'attachment',
                '_wp_attachment_metadata',
                '%' . $wpdb->esc_like('"s3"') . '%',
            ]
        )));
    }
}
