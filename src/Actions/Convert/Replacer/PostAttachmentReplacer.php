<?php namespace WpsMcloud\Actions\Convert\Replacer;

use WpsMcloud\Models\PostAttachment;
use WpsMcloud\Actions\Convert\ReplacerService;
use WpsMcloud\Exceptions\UrlDetectException;

class PostAttachmentReplacer
{
    private PostAttachment $post;

    private ReplacerService $replacer;

    public string $mcloudPostUrl;
    public string $mcloudPostBaseUrl;
    public string $localPostBaseUrl;

    /**
     * @var string
     * @desc Filename with prepend parts after "wp-content/uploads/"
     */
    public string $key;

    /**
     * @var string Only prepends parts without filename
     */
    public string $baseKey;

    public function __construct(PostAttachment $post, ReplacerService $replacer)
    {
        $this->post = $post;

        $this->replacer = $replacer;

        $this->mcloudPostUrl = str_replace(
            $replacer->getUploadsDirBaseUrl(),
            $replacer->getMcloudBaseUrl(),
            $post->getUrl()
        );

        if ($this->mcloudPostUrl == $post->getUrl()) {
            throw new UrlDetectException(
                sprintf(<<<EOF
                    Your site url has manual changed in wp-config and not appropriate with post attachments guids.
                    Guid: %s
                    Manual url: %s
                    
                    EOF,
                    $post->getUrl(),
                    $replacer->getUploadsDirBaseUrl()
                )
            );
        }

        $this->mcloudPostBaseUrl = trailingslashit(
            str_replace($this->post->getBasename(), '', $this->mcloudPostUrl)
        );

        $this->localPostBaseUrl = trailingslashit(
            str_replace($this->post->getBasename(), '', $this->post->getUrl())
        );

        $this->key = ltrim(
            str_replace($replacer->getMcloudBaseUrl(), '', $this->mcloudPostUrl),
            '/',
        );

        $this->baseKey = ltrim(
            trailingslashit(
                str_replace(
                    $this->post->getBasename(),
                    '',
                    $this->key
                )
            ),
            '/'
        );
    }

    public function getGeneralS3Info(): array
    {
        return $this->getS3Info($this->getMcloudPostUrl(), $this->getKey());
    }

    public function getSizeS3Info(string $sizeFilename): array
    {
        return $this->getS3Info(
            $this->getMcloudPostBaseUrl() . $sizeFilename,
            $this->getBaseKey() . $sizeFilename
        );
    }

    private function getS3Info(string $url, string $key): array
    {
        return [
            'url' => $url,
            'bucket' => $this->replacer->getBucket(),
            'provider' => 's3',
            'privacy' => ReplacerService::S3_ACL,
            'v' => MEDIA_CLOUD_INFO_VERSION,
            'key' => $key,
            'options' => [],
            'mime-type' => $this->post->getPostMimeType(),
        ];
    }

    public function getMcloudPostUrl(): string
    {
        return $this->mcloudPostUrl;
    }

    public function getMcloudPostBaseUrl(): string
    {
        return $this->mcloudPostBaseUrl;
    }

    public function getLocalPostBaseUrl(): string
    {
        return $this->localPostBaseUrl;
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function getBaseKey(): string
    {
        return $this->baseKey;
    }
}
