<?php namespace WpsMcloud\Actions\Convert;

use WpsMcloud\Support\Http;
use WpsMcloud\Support\Counters;
use WpsMcloud\Actions\Convert;
use WpsMcloud\Models\PostAttachment;

class ReplacerService
{
    const S3_ACL = 'public-read';

    private string $uploadsDirBaseUrl;
    private string $mcloudEndPoint;
    private string $bucket;

    private string $mcloudBaseUrl;

    public function __construct()
    {
        $this->uploadsDirBaseUrl = trailingslashit(wp_get_upload_dir()['baseurl']);
        $this->mcloudEndPoint = trailingslashit(get_option('mcloud-storage-s3-endpoint'));
        $this->bucket = get_option('mcloud-storage-s3-bucket');

        $this->mcloudBaseUrl = trailingslashit( $this->getMcloudEndPoint() . $this->bucket);
    }

    public function processImage(PostAttachment $post): void
    {
        if (!Http::fileExists($post->getUrl())) {
            $post->setFlaggedAs404();

            echo sprintf('Not found image %d: %s', $post->getId(), $post->getUrl());

            Counters::$imagesNotFound++;

            return;
        }

        $postAttachmentReplacer = new Convert\Replacer\PostAttachmentReplacer($post, $this);

        $meta = $post->getMeta();

        $meta['file'] = $postAttachmentReplacer->getKey();
        $meta['s3'] = $postAttachmentReplacer->getGeneralS3Info();

        if (array_key_exists('sizes', $meta) && is_array($meta['sizes'])) {
            $newSizes = [];

            foreach ($meta['sizes'] as $size => $sizeData) {
                $sizeFilename = $sizeData['file'];

                $localSizeUrl = $postAttachmentReplacer->getLocalPostBaseUrl() . $sizeFilename;

                if (!Http::fileExists($localSizeUrl)) {
                    echo sprintf(
                        'Not found size "%s" image %d: %s',
                        $sizeFilename,
                        $post->getId(),
                        $localSizeUrl
                    ) . PHP_EOL;

                    Counters::$imagesNotFound++;

                    continue;
                }

                $sizeData['s3'] = $postAttachmentReplacer->getSizeS3Info($sizeFilename);

                $newSizes[$size] = $sizeData;
            }

            $meta['sizes'] = $newSizes;
        }

        $post->setAttachmentMetaData($meta);

        Counters::$processedImages++;
    }

    public function getUploadsDirBaseUrl(): string
    {
        return $this->uploadsDirBaseUrl;
    }

    public function getMcloudEndPoint(): string
    {
        return $this->mcloudEndPoint;
    }

    public function getBucket(): string
    {
        return $this->bucket;
    }

    public function getMcloudBaseUrl(): string
    {
        return $this->mcloudBaseUrl;
    }

}
