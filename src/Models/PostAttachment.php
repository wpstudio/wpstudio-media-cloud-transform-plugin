<?php namespace WpsMcloud\Models;

class PostAttachment
{
    const META_KEY_FLAGGED_AS_404 = '_wp_media_cloud_transform_404';
    const META_KEY_ATTACHMENT_METADATA = '_wp_attachment_metadata';

    public int $id;
    public string $meta_value;
    public ?array $meta;
    public string $guid;
    public string $post_mime_type;

    public function __construct($post)
    {
        $this->id = $post->ID;
        $this->meta_value = $post->meta_value;
        $this->guid = $post->guid;
        $this->post_mime_type = $post->post_mime_type;
    }

    public static function get(array $posts): array
    {
        return array_map(fn ($post) => new self($post), $posts);
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getMeta(): array
    {
        if (!isset($this->meta)) {
            $this->meta = unserialize($this->meta_value);
        }

        return $this->meta;
    }

    public function getUrl(): string
    {
        return $this->guid;
    }

    public function getBasename(): string
    {
        return mb_basename($this->getUrl());
    }

    public function getPostMimeType(): string
    {
        return $this->post_mime_type;
    }

    public function setAttachmentMetaData(array $meta)
    {
        update_post_meta($this->getId(), self::META_KEY_ATTACHMENT_METADATA, $meta);
    }

    public function setFlaggedAs404()
    {
        add_post_meta($this->getId(), self::META_KEY_FLAGGED_AS_404, 1);
    }
}
