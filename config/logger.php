<?php

return [
    /**
     * Available logging types: html, error_log, both
     * default: html
     */
    'log_type' => $_ENV['LOG_TYPE'] ?? 'html',
];
